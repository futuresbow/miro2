<?php

class Kereses_modul extends MY_Modul {
	public function kereso() {
		
		$keresoSzo = urldecode($this->ci->uri->segment(2));
		$keresoSzavak = explode(' ',trim($keresoSzo));
		$whereArr = array();
		$whereArr2 = array();
		foreach($keresoSzavak as $szo) {
			$szo = trim($szo);
			if($szo != '') {
				$whereArr[] =  " cim LIKE '%$szo%' ";
				$whereArr2[] =  " uzenet LIKE '%$szo%' ";
			}
		}
		$eredmenyek = array();
		$eredmenyek2 = array();
		if(!empty($whereArr)){
			
			$sql = "SELECT id FROM cikkek WHERE MATCH(cim) AGAINST ('$keresoSzo' IN NATURAL LANGUAGE MODE)";
			$eredmenyek = $this->Sql->sqlSorok($sql);
			//print_r($eredmenyek);
			
			if(empty($eredmenyek)) {
				$w = implode(' and ', $whereArr);
				
				$sql = "SELECT DISTINCT(id) 
					FROM cikkek 
					WHERE $w
					LIMIT 50";
				 //print $sql;
				$eredmenyek = $this->Sql->sqlSorok($sql);
				// print '2: ';
				// print_r($eredmenyek);
			}
			
			
			//hozzászólások?
			
			$sql = "SELECT cikk_id id FROM  hozzaszolasok WHERE MATCH(uzenet) AGAINST ('$keresoSzo' IN NATURAL LANGUAGE MODE)";
			$eredmenyek2 = $this->Sql->sqlSorok($sql);
			//print_r($eredmenyek);
			
			if(empty($eredmenyek)) {
				$w = implode(' and ', $whereArr2);
				
				$sql = "SELECT DISTINCT(cikk_id) as id 
					FROM  hozzaszolasok 
					WHERE $w
					LIMIT 50";
				 //print $sql;
				$eredmenyek2 = $this->Sql->sqlSorok($sql);
				// print '2: ';
				 //print_r($eredmenyek2);
			}
			
		}
		$tema = FRONTENDTEMA;
		$data['modulKimenet'] = $this->load->view($tema.'html/kereses_view', array('keresoSzo' => $keresoSzo, 'eredmenyek' => $eredmenyek, 'eredmenyek2' => $eredmenyek2), true);
		
		if(globalisMemoria('template_feluliras')) $tema = globalisMemoria('template_feluliras').'/';
		
		
		$this->load->view($tema.'keret_view', $data);
	}
	
	public function index() {
		
		$data['keresoSzo'] = $keresoSzo = $this->ci->uri->segment(2);
		
		naplozo('Keresés', 0, '',$keresoSzo);
		
		global $keresesiPontok;
		if(empty($keresesiPontok)) ws_moduladatok();
		$eredmenyek = array();
		foreach($keresesiPontok as $eleres) {
			$eleres = explode('/', $eleres);
			if(@$eleres[0]=='' or @$eleres[1]=='' or @$eleres[2]=='') continue;
			include_once(FCPATH.'modules/'.$eleres[0].'/'.$eleres[1].'.php');
			$o = new $eleres[1];
			
			$eredmenyek[$eleres[0].'_'.$eleres[1].'_'.$eleres[2]] = $o->{$eleres[2]}($keresoSzo);
		}
		
		$tema = FRONTENDTEMA;
		$data['modulKimenet'] = $this->load->view($tema.'html/kereses_view', array('keresoSzo' => $keresoSzo, 'eredmenyek' => $eredmenyek), true);
		
		if(globalisMemoria('template_feluliras')) $tema = globalisMemoria('template_feluliras').'/';
		
		$this->load->view($tema.'keret_view', $data);
	}
}
