<?php

class Frontendlapok extends MY_Modul {
	
	
	public function fooldalitartalmak($param=null) {
		
		$ci = getCI();
		globalisMemoria('bodyclass', 'home');
		naplozo('Főoldal megtekintése');
		
		if($this->ci->uri->segment(1)) return widget('forum/forum/kerdes', false);
		
		return $this->ci->load->view(FRONTENDTEMA.'html/fooldal', array('param' => $param), true);

	}
	
	
	
}
