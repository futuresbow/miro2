<?php

$tartalomkezeloAdatok['forum/forum/cikkiras'] = array(

	'cim' => 'Cikkek írása',
	'jogkorok' => JOG_SUPERADMIN,

);
$tartalomkezeloAdatok['forum/forum/kerdes'] = array(

	'cim' => 'Kérdés oldal',
	'jogkorok' => JOG_SUPERADMIN,

);
$tartalomkezeloAdatok['forum/forum/kerdezz'] = array(

	'cim' => 'Kérdésfeltevés oldala',
	'jogkorok' => JOG_SUPERADMIN,

);
$tartalomkezeloAdatok['forum/forum/temakorok'] = array(

	'cim' => 'Témakörök oldala',
	'jogkorok' => JOG_SUPERADMIN,

);
$tartalomkezeloAdatok['forum/forum/temakor'] = array(

	'cim' => 'Témak oldala',
	'jogkorok' => JOG_SUPERADMIN,

);

$modulAdatok[] = (object)array(

	'eleresek' => array('hszmentes' => 'hozzaszolasmentes'),

	'nev' => 'forum'

);
ws_autoload('forum');
