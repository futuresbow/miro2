<div class="modal fade" id="regModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Regisztráció</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="regModalForm">
			
		 <div id="regResp"></div>
			
         <input type="hidden" id="regClassBackField" name="callback" value="<?= $callBack;?>">
         <div class="form-group">
			<label for="exampleInputEmail1">Becenév</label>
			<input name="reg[nick]" type="text" class="form-control"  >
			
		  </div>
		   <div class="form-group">
			<label for="exampleInputEmail1">Valós E-mail címed</label>
			<input name="reg[email]" type="email" class="form-control"  >
			
		  </div>
		  
		  <div class="form-group">
			<label for="exampleInputPassword1">Jelszó</label>
			<input name="reg[jelszo]" type="password" class="form-control" >
			
		  </div>
         
			
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
        <button type="button" class="btn btn-primary" onclick="siteJs.regModal();" >Regisztrálok</button>
      </div>
    </div>
  </div>
</div>
