
<section class="mbr-section content4 max-800" id="content4-12" data-rv-view="5939">

    

    <div class="container">
        <div class="media-container-row ">
            <div class="title ">
				<div class="row">
					<div class="col-10">
						<p class="mbr-section-subtitle mbr-light mbr-fonts-style">
							<b><a href="<?= base_url().'szerzok/'.$cikk->szerzo->nick; ?>"><?= $cikk->szerzo->nick;?></a> kérdése</b> 
						</p>
						<h1 class="align-center pb-3 mbr-fonts-style display-5">
							
							<?php $tag = ws_belepesEllenorzes();if($tag) if($tag->adminjogok>0 or ($cikk->vanHozzaszolas()==false and $tag->id == $cikk->szerzo->id) ):?>
							<a onclick="if(!confirm('Tuti?'))return false;"  class="btn btn-danger btn-sm float-right" style="matgin-left:5px;" href="<?= base_url().'cikkiras/'.$cikk->id ?>?delthis=1">törlés</a>
							
							<a class="btn btn-primary btn-sm float-right" href="<?= base_url().'cikkiras/'.$cikk->id ?>">szerkesztés</a>
							<?php endif;?>
							<span  class="text-break" >
							<?= $cikk->teljescim(); ?> 
							</span>
							
						</h1>
						
					</div>
					<div class="col-2">
						<span class="kommbtn"><i class="fas fa-eye"></i> <?= $cikk->megtekintesek ?></span><span class="kommbtn" data-like="<?= $cikk->kedvelesek ?>" onclick="siteJs.like(<?= $cikk->id; ?>, this)"><i class="fas fa-thumbs-up"></i> <span><?= $cikk->kedvelesek ?></span></span>
						
					</div>
                </div>
               
                <div class="mbr-text mbr-fonts-style text-justify">
                
                
                
                <?php if($cikk->vanHozzaszolas()) foreach($cikk->hozzaszolasok as $hsz):?>
                <article class="clearfix">
					<p><b><a href="<?= base_url().'szerzok/'.$hsz->nick; ?>"><?= $hsz->nick;?></a></b> 
					<?php $tag = ws_belepesEllenorzes();if($tag) if($tag->adminjogok>0 or ( $tag->id == $hsz->fid) ):?>
					<a class="float-right" onclick="siteJs.hszEdit(<?= $hsz->id;?>)" href="javascript:void(0);">válasz szerkesztése</a>
					<?php endif;?>
					</p>
					<p class="uz<?= $hsz->id;?>"><?= nl2br($hsz->uzenet); ?></p>
					<?php $tag = ws_belepesEllenorzes();if($tag) if($tag->adminjogok>0 or ( $tag->id == $hsz->fid) ):?>
					<div class="mentesgombdiv" style="display:none"><button class="mentes btn btn-success btn-sm" onclick="siteJs.hszSave(<?= $hsz->id;?>)">Módosítás</button></div>
					<?php endif;?>
					
					
					<?php $tag = ws_belepesEllenorzes(); if($hsz->elsovalasz!=1) if($tag) if($tag->adminjogok>0 or ( $tag->id == $hsz->fid) ):?>
					<a class="float-right btn btn-danger btn-sm" href="?del=<?= $hsz->id; ?>" onclick="if(!confirm('Biztosan? Végleges lesz a törlés...')) return false;">Törlöm ezt a tartalmat</a>
					<?php endif; ?>
				
				</article>
				<hr>
                <?php endforeach;?>
				</div>
				
				<form method="post">
				
				<div class="card bg-light mb-12" >

	  <div class="card-body">
		<h5 class="card-title">Te mit gondolsz?</h5>
		<div class="form-group">
		<textarea class="form-control" name="hsz" required id="exampleFormControlTextarea1" rows="3"></textarea>
		</div>
		
	
	  <?php if(!$tag):?>

		<div class="belepoform">
			
			
			<div class="row">
				<div class="col-sm">
				  <a href="javascript:void(0);" onclick="$('#loginModal').modal();" class="btn btn-success  btn-block">LÉPJ BE, ha már van fiókod...</a>
				</div>
				<div class="col-sm">
				  <a href="javascript:void(0);" onclick="$('#regModal').modal();" class="btn btn-info  btn-block">REGISZTRÁLJ, ha új vagy itt!</a>
				</div>
				
			</div>
			
			
		 </div>
		 
		<?php else: ?>

		<?php if($tag->adminjogok>0):?>
		
		<div class="form-group">
			<label for="cimkek">Felhasználónév</label>
			<input type="text"  class="form-control" name="felhasznalonev" id="cimkek" >
		</div>
		<?php endif; ?>


		<?php endif; ?>
		<div class="regLeiras" style="display:none">
		<p>A regisztrációd után aktivációs levelet küldtünk Neked. Kérlek, kattints az aktiváló linkre a levélben. Így tudod biztosítani, hogy írásod folyamatosan olvasható legyen az oldalon.</p>
		</div>

		<div class="custom-file hidden filefeltolto">
		  <input name="kep" type="file" class="custom-file-input" id="customFile">
		  <label  class="custom-file-label" for="customFile">Kép feltöltése (ügyelj a méretre, maximum 2 MB, 1000px * 800px)</label>
		</div>
		<p><br /></p>
		<button type="button" onclick="$('.filefeltolto').toggleClass('hidden');" class="btn btn-secondary btn-sm kuldesGomb float-right" style="<?php if(!$tag):?>display:none;<?php endif;?>">További lehetőségek</button>

		
		<button type="submit" class="btn btn-barna kuldesGomb" style="<?php if(!$tag):?>display:none;<?php endif;?>">Közzéteszem</button>
			
		
  
  </div>
</div>
            
				</form>
            </div>
        </div>
    </div>
</section>

