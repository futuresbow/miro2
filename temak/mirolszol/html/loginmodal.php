<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Belépés</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="loginModalForm">
			
		 <div id="logResp"></div>
			
         <input type="hidden" id="loginClassBackField"  name="callback" value="<?= $callBack;?>">
         <div class="form-group">
			<label for="exampleInputEmail1">Becenév vagy E-mail</label>
			<input name="login[nick]" type="text" class="form-control"  >
			
		  </div>
		  
		  <div class="form-group">
			<label for="exampleInputPassword1">Jelszó</label>
			<input name="login[jelszo]" type="password" class="form-control" >
		  </div>
         
			
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Mégsem</button>
        <button type="button" class="btn btn-primary" onclick="siteJs.loginModal();" >Belépek</button>
      </div>
    </div>
  </div>
</div>
