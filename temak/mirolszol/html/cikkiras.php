
<form method="post">


 <p><strong>
	<?php if(!$kerdesfelteves):?>
	Mi a kérdés, amit Te válaszolsz meg először?
	<?php else:?>
	Mi a kérdésed?
	<?php endif;?>
</strong></p>
<div class="input-group mb-3">
	
 
	
  <div class="input-group-prepend">
     <select class="custom-select" name="a[elotag]" id="inputGroupSelect02">
		<option value="Miről szól" <?= @$cikk['elotag']=='Miről szól'?' selected ':'';?> >Miről szól</option>
		<option value="Miről szólnak" <?= @$cikk['elotag']=='Miről szólnak'?' selected ':'';?> >Miről szólnak</option>
		<?php /*
		<option value="1">Mi az értelme a(z) </option>
		<option value="2">Mitől jó a(z) </option>
		<option value="3">Mit jelent a(z) </option>
		*/?>
	  </select>
	 
  </div>
  
  <input type="text" name="a[cim]" value="<?= htmlspecialchars(@$cikk['cim']);?>" placeholder="Írd ide a kérdést" class="form-control" aria-label="Text input with dropdown button">

	 <div class="input-group-append">
			<span class="input-group-text formelotag" id="basic-addon2"> ? </span>
		  </div>

</div>
<?php if(!$edit):?>
<?php if(!$kerdesfelteves):?>
<div class="form-group">
    <label for="cikk">A Te válaszod: <small>(az következő hozzászólásig módosíthatod)</small></label>
    <textarea  name="a[cikk]" class="form-control cikktext" id="exampleFormControlTextarea1" rows="3"><?= @($cikk['cikk']);?></textarea>
</div>
<?php endif; ?> 
<?php endif; ?> 

<div class="form-group">
	<label>Milyen témákba sorolnád a kérdést?</label>
	
	<div class="row">
		<div class="col-3">
			<select class="form-control" onchange="labelHozzaadas(this)" >
				<option value="0">Válassz cimkét</option>
				<?php $cimkek = $this->Sql->gets("kerdeskategoriak", "ORDER BY bejegyzesszam ASC "); foreach($cimkek as $sor): ?>
				<option class="mondatkezdo" value="<?= $sor->id;?>" class="opcio<?= $sor->id;?>" style="<?php if(isset($cimkevalasztas[$sor->id])) echo ('display:none');?>"><?= ucfirst($sor->nev); ?></option>
				<?php endforeach;?>
			</select>
		</div>
		<div class="col cimketar">
			Kiválasztott cimkék: 
			<?php if(isset($cimkevalasztas)) foreach($cimkek as $sor): if(!isset($cimkevalasztas[$sor->id])) continue;?>
			<input type="text" readonly class="btn btn-secondary btn-sm cimkebtn mondatkezdo" onclick="delthis(this)" data-id="<?= $sor->id?>" name="cimkevalasztas[<?= $sor->id?>]" style="width:auto;" value="<?= $sor->nev?> X">
			<?php endforeach; ?>
		</div>
	</div>
	

</div>
<script>
$().ready(function(){ $('.selectpicker').selectpicker(); })
function labelHozzaadas(o) {
	opcio = o.options[o.selectedIndex];
	label = $(opcio).html();
	id = parseInt($(opcio).val());
	if(id==0) return;
	$('.opcio'+id).hide();
	
	$('.cimketar').append('<input type="text" readonly class="btn btn-secondary btn-sm cimkebtn mondatkezdo" onclick="delthis(this)" data-id="'+id+'" name="cimkevalasztas['+id+']" style="width:auto;" value="'+label+' X">');
	
}
function delthis(o) {
	id = $(o).attr('data-id');
	$('.opcio'+id).show();
	$(o).remove();
}
</script>




<?php if(!$tag):?>

<div class="belepoform">
	
	
	<div class="row">
		<div class="col-sm">
		  <a href="javascript:void(0);" onclick="siteJs.formBelepo();" class="btn btn-success  btn-block">LÉPJ BE, ha már van fiókod...</a>
		</div>
		<div class="col-sm">
		  <a href="javascript:void(0);" onclick="siteJs.formReg();" class="btn btn-info  btn-block">REGISZTRÁLJ, ha új vagy itt!</a>
		</div>
		
	</div>
	
	
 </div>
 
<?php else: ?>

<?php if($tag->adminjogok>0):?>
<div class="form-group">
    <label for="cimkek">Még nem létező cimke hozzáadása (felsorolás, vesszővel elválasztva, pl. "autó, szerelés, szakmunka" )</label>
    <input type="text" class="form-control" name="cimkek" id="cimkek" >
</div>

<div class="form-group">
    <label for="cimkek">Felhasználónév</label>
    <input type="text"  class="form-control" name="felhasznalonev" id="cimkek" >
</div>
<?php endif; ?>


<?php endif; ?>
<div class="regLeiras" style="display:none">
<p>A regisztrációd után aktivációs levelet küldtünk Neked. Kérlek, kattints az aktiváló linkre a levélben. Így tudod biztosítani, hogy írásod folyamatosan olvasható legyen az oldalon.</p>
</div>

<div class="custom-file hidden filefeltolto">
  <input name="kep" type="file" class="custom-file-input" id="customFile">
  <label  class="custom-file-label" for="customFile">Kép feltöltése (ügyelj a méretre, maximum 2 MB, 1000px * 800px)</label>
</div>
<p><br /></p>
<button type="button" onclick="$('.filefeltolto').toggleClass('hidden');" class="btn btn-secondary btn-sm kuldesGomb float-right" style="<?php if(!$tag):?>display:none;<?php endif;?>">További lehetőségek</button>

<?php if($edit):?>
<button type="submit" class="btn btn-barna kuldesGomb" style="<?php if(!$tag):?>display:none;<?php endif;?>">Módosítom</button>
<?php else: ?>

<button type="submit" class="btn btn-barna kuldesGomb" style="<?php if(!$tag):?>display:none;<?php endif;?>">Közzéteszem <?php if(!$kerdesfelteves):?>
	 az írásomat
	<?php else:?>
	a kérdést
	<?php endif;?></button>
	
	
<?php endif; ?>



</form>
<br><br><br><br><br><br><br>
