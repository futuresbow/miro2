<h1>Keresés eredménye: <?=  $keresoSzo; ?></h1>
<div class="row">
      <div class="col-lg-5 border-right position-relative kerdesparagrafus">
        <h4 class="text-secondary kishead">Találatok a kérdések között</h4>
        
        <?php if($eredmenyek): foreach($eredmenyek as $sor): $cikk = new Cikk_osztaly($sor->id);?>
        <table class="liketable ">
			<tr>
				<td><a href="<?= $cikk->link();?>" class="text-break"  ><?= $cikk->teljescim();?></a></td>
				<td class="text-nowrap align-bottom" style="width:20px"><span class="kommbtn"><i class="fas fa-eye"></i> <?= $cikk->megtekintesek ?></span><span class="kommbtn" data-like="<?= $cikk->kedvelesek ?>" onclick="siteJs.like(<?= $cikk->id; ?>, this)"><i class="fas fa-thumbs-up"></i> <span><?= $cikk->kedvelesek ?></span></span></td>
			</tr>
        </table>
       
        
        <?php endforeach; else: ?>
        <div class="alert alert-success">Sajnos nincs találat</div>
        <?php endif; ?>
         
       
        
        <p class="text-center bottomlink"><a class="btn btn-lg" href="javascript:void(0);" role="button"><i class="fas fa-chevron-down"></i></a></p>
      
      </div>
      
      <div class="col-lg-5 border-right position-relative kerdesparagrafus">
        <h4 class="text-secondary kishead">Találatok a hozzászólások között</h4>
        
        <?php if($eredmenyek2): foreach($eredmenyek2 as $sor): $cikk = new Cikk_osztaly($sor->id);?>
        <table class="liketable ">
			<tr>
				<td><a href="<?= $cikk->link();?>" class="text-break"  ><?= $cikk->teljescim();?></a></td>
				<td class="text-nowrap align-bottom" style="width:20px"><span class="kommbtn"><i class="fas fa-eye"></i> <?= $cikk->megtekintesek ?></span><span class="kommbtn" data-like="<?= $cikk->kedvelesek ?>" onclick="siteJs.like(<?= $cikk->id; ?>, this)"><i class="fas fa-thumbs-up"></i> <span><?= $cikk->kedvelesek ?></span></span></td>
			</tr>
        </table>
       
        
        <?php endforeach; else: ?>
        <div class="alert alert-success">Sajnos nincs találat</div>
        <?php endif; ?>
        
       
        
        <p class="text-center bottomlink"><a class="btn btn-lg" href="javascript:void(0);" role="button"><i class="fas fa-chevron-down"></i></a></p>
      
      </div>
      
      
      
      <div class="col-lg-2 text-center">
        
         <h4 class="text-secondary kishead">Van egy jó témád?</h4>
        
        <p >
			<a href="<?= base_url();?>cikkiras" class="btn btn-barna btn-lg btn-block">Írj egy cikket</a>
        </p>
       
        <p>&nbsp;</p>
         <h4 class="text-secondary kishead">Választ keresel?</h4>
        
        <p >
			<a href="<?= base_url();?>kerdezz" class="btn btn-barna btn-lg btn-block">Itt kérdezhetsz</a>
        </p>
        
       
        
        
      </div>
    </div>

    <hr>
	

