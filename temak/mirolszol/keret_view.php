<?php include 'tema_valtozok.php';?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8" />
	<title><?= ws_seo('cim'); ?></title>
	<meta name="description" content="<?= ws_seo('leiras'); ?>">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= base_url() ?>temak/mirolszol/css/main.css" >
	<link rel="stylesheet" href="<?= base_url() ?>temak/mirolszol/css/extra.css" >
	<link href="<?= base_url() ?>temak/fa/css/solid.min.css" rel="stylesheet"> 
	<link href="<?= base_url() ?>temak/fa/css/fontawesome.min.css" rel="stylesheet"> 
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css" />


	<script src="//code.jquery.com/jquery-latest.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-164677043-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-164677043-1');
</script>

	
	
</head>
<body>
	<div class="loading">
	</div>
	<?= widget('felhasznalok/felhasznalok/loginmodal', array('callBack'=> 'siteJs.belepesVege()'));?>
	<?= widget('felhasznalok/felhasznalok/regmodal', array('callBack'=> 'siteJs.regisztracioVege()'));?>
	
	

	<div class="jumbotron keresohatter">
    <div class="container text-center formcontainer">
      <h1 onclick="window.location='<?= base_url();?>'" class="display-4">Mirolszol.Com</h1>
      <p>
		<div class="input-group input-group-lg ">
		  <div class="input-group-prepend">
			 <span class="input-group-text formelotag">Miről szól a(z) </span>
		  </div>
		  <input onkeyup="if(event.keyCode === 13) { window.location.href='<?= base_url().'kereses/'?>'+$(this).val(); } "  type="text" class="form-control" id="mirolszolkerdes"  aria-describedby="basic-addon3" value="<?= isset( $keresoSzo)?$keresoSzo:'';?>">
		  <div class="input-group-append">
			<span class="input-group-text formelotag" id="basic-addon2"> ? </span>
		  </div>
		  
		  <div class="input-group-append">
			<button onclick="window.location.href='<?= base_url().'kereses/'?>'+$('#mirolszolkerdes').val();" class="btn btn-outline-secondary btn-barna" type="button" id="button-addon2"><i class="fas fa-search"></i></button>
		  </div>
		  
		</div>
      </p>
    </div>
    
    
   
    
  </div>
	
	<div class="container <?php $ci = getCI(); if($ci->uri->segment(1)=="") print 'fooldalkontener'?>">
	
	<nav class="navbar navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav mr-auto">	
				<li class="nav-item">
					<a  href="<?= base_url(); ?>"  class="nav-link ">Főoldal</a>
				</li>
				<?php foreach($this->Sql->gets("kerdeskategoriak", "WHERE menupont = 1 ORDER BY bejegyzesszam ASC LIMIT 10") as $sor):?>
					<li class="nav-item">
						<a  href="<?= base_url(); ?>temakor/<?= $sor->slug; ?>"  class="nav-link menupontok "><?= $sor->nev; ?></a>
					</li>
				<?php endforeach;?>
				<li class="nav-item">
					<a href="<?= base_url(); ?>temakorok"  class="nav-link ">Összes témakör</a>
				</li>
			  
			</ul>
			<span class="navbar-text">
			<?php $tag = ws_belepesEllenorzes(); if($tag):?>
			<?php if($tag->adminjogok>0):?>
			<a href="<?= base_url().beallitasOlvasas('ADMINURL');?>">Admin</a> |  
			
			<?php endif;?>
			<a href="<?= base_url()?>?logout">Kilépés</a> 
			
			Üdvözöllek <?= $tag->nick; ?>! 
			
			<?php else:?>
			<a href="javascript:void(0);" onclick="siteJs.belepes();">Belépés</a> |  
			<a href="javascript:void(0);" onclick="siteJs.regisztracio();">Regisztráció</a>  
			
			<?php endif;?>
			</span>
		</div>
	</nav>
	</div>
	
	<div class="container maincontener <?php $ci = getCI(); if($ci->uri->segment(1)=="") print 'fooldalkontener'?>">
		
		<?php $uzenetek = globalisMemoria('hibaUzenetek');if($uzenetek) foreach($uzenetek as $uzenet):?>
		<div class="alert alert-<?= $uzenet['tipus'];?>"><?= $uzenet['txt'];?></div>
		<?php endforeach;?>
		<?= $modulKimenet; ?>
	</div>
 	
	
	
	
	
	
		
		
		
		
		
		
		
		
		
		<div class="seo-content">
			<div class="wrap">
				<div class="seo-txt">
					<?= nl2br(ws_seo('hosszuleiras'));?>
				</div>
			</div>
		</div>
		
		
		<footer class="container">
			<p class="float-right"><a href="#">Ugrás az oldal tetejére</a></p>
			<p>© <?= date('Y')?> Mirolszol.com  
			<?php foreach(ws_frontendMenupontok() as $sor):?>
			<a href="<?= base_url().$sor->url;?>" title="<?= $sor->felirat;?>" class="<?= $sor->aktiv==true?'active':'';?>"> · <?= $sor->felirat; ?></a>
			<?php endforeach; ?>
		</footer>
  
		
		

	</footer>
	<!-- end: footer -->
		
	<?php include(FCPATH.'temak/kozos_elemek/js/sitejs.php'); ?>
	
</body>
</html>




	
