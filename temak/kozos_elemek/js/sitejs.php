<script>
function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function base_url() { return '<?= base_url(); ?>';}
	
	</script>
	<script>
var siteJs = {};


	siteJs.like = function(id, o) {
		$().post('<?= base_url(); ?>cikklike', {id:id}, function(r){
			if(r!='0') {
				
				$(o).html('<i class="fas fa-thumbs-up"></i> <span>'+r+'</span>');
				
			}
		})
	}
	siteJs.oldalFrissites = function() {
		window.location.href = window.location.href;
	}
	siteJs.formBelepo = function() {
		$('#loginModal').modal();
		$('input[name="callback"]').val('siteJs.belepesVege()');
	}
	siteJs.formReg = function() {
		$('#regModal').modal();
		$('input[name="callback"]').val('siteJs.belepesVege()');
	}
	siteJs.belepes = function() {
		$('#loginModal').modal();
		$('input[name="callback"]').val('siteJs.oldalFrissites()');
	}
	siteJs.regisztracio = function() {
		$('#regModal').modal();
		$('input[name="callback"]').val('siteJs.oldalFrissites()');
	}
	siteJs.hszEdit = function(id) {
		//contenteditable="true"
		$('.uz'+id).attr('contenteditable', "true").addClass('editing').next('.mentesgombdiv').show();
		
	}
	siteJs.hszSave = function(id) {
		//contenteditable="true"
		$('.uz'+id).prop('contenteditable', false).removeClass('editing').next('.mentesgombdiv').hide();
		$.post('<?= base_url();?>hszmentes', {id:id, uzenet:$('.uz'+id).html()});
	}
	siteJs.fatyolStop = function() {
		$('.loading').fadeOut(400);
	}
	siteJs.fatyolStart = function() {
		$('.loading').show();
	}
	siteJs.loginModal = function(callBack) {
		$.post('<?= base_url();?>loginmodal', $('#loginModalForm').serialize(), function(r) { $('#logResp').html(r);} );
	}
	siteJs.regModal = function(callBack) {
		$.post('<?= base_url();?>regmodal', $('#regModalForm').serialize(), function(r) { $('#regResp').html(r);} );
	}
	siteJs.belepesVege = function() {
		
		$('.belepoform').slideUp();
		$('.kuldesGomb').fadeIn(2000);
		$('#loginModal').modal('hide')
		
	}
	siteJs.regisztracioVege = function() {
		
		$('.belepoform').slideUp();
		$('.regLeiras').fadeIn(2000);
		$('.kuldesGomb').fadeIn(2000);
		$('#regModal').modal('hide')
		
	}
	siteJs.validateEmail = function(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
	
	$().ready(function(){ siteJs.fatyolStop(); window.onbeforeunload = function(event) {  siteJs.fatyolStart(); };});
	</script>
