
<div class="static ">

    <h1><?= __f('Elfelejtett jelszó visszaállítása');?></h1>
    <p class="lead">
        <?= __f('Kérjük, add meg E-mail címedet');?>
    </p>

    <?php if($hiba==true) print '<h4 style="color:red">'.__f('Nem megfelelő E-mail cím, vagy nem szerepel az adatbázisban.').'</h4>';?>


        <form method="post" id="urlap">
			
			
			<div class="">

				<div class="">
					<div class="form-group ">
						<label class="important"><?= __f('E-mail cím');?></label>
						<input class="form-control"  type="email" id="email" name="elfelejtett_email" value="<?= @$_POST['elfelejtett_email']; ?>">
						<div class="form-text text-muted" style="display:none"> <?= __f('Hiba!');?></div>
					</div>
				</div>

				

			</div>
            

           

            <div class="form-group">
                <button type="submit"  class="btn btn-info">
                    <?= __f('Jelszó visszaállítása'); ?>
                </button>
            </div>
        </form>

</div>
