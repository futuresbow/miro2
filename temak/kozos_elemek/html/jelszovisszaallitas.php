  <div class="static">

    <h1><?= __f('Jelszó visszaállítás');?></h1>
	<?php if($kizaroUzenet):?>
    <p class="alert alert-info">
        <?= $kizaroUzenet; ?>
    </p>
    <?php else:?>
    
    
    <?php if($jelszohiba):?>
    <div class="alert alert-danger">
        <?= $jelszohiba; ?>
    </div>
    <?php endif;?>
    
      <form method="post" id="urlap">
			<div class="form-elements">

				<div class="form-row">
					<div class="form-group ">
						<label class="important"><?= __f('Új jelszó');?> <small><?= __f('legalább 6 karakter hosszú'); ?></small></label>
						<input class="form-control" type="password" id="pwd1" name="pwd1" value="" placeholder="******">
						<div class="error-msg"> <?= __f('Hiba!');?></div>
					</div>
				</div>

				
				<div class="form-row">
					<div class="form-group ">
						<label class="important"><?= __f('Jelszó újra');?> <small></small></label>
						<input class="form-control" type="password" id="pwd2" name="pwd2" value="" placeholder="******">
						<div class="error-msg"> <?= __f('Hiba!');?></div>
					</div>
				</div>

				

			</div>
            

           

            <div class="form-btn-container">
                <button type="submit"  class="btn btn-info">
                    <?= __f('Jelszó visszaállítása'); ?>
                </button>
            </div>
        </form>
        
        
    <?php endif;?>
 </div>
 
 

